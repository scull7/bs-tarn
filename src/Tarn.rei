type t;

let make:
  (
    ~min: int=?,
    ~max: int=?,
    ~acquireTimeoutMillis: int=?,
    ~createTimeoutMillis: int=?,
    ~idleTimeoutMillis: int=?,
    ~reapIntervalMillis: int=?,
    ~createRetryIntervalMillis: int=?,
    ~propagateCreateError: bool=?,
    ~create: ((Js.Nullable.t(exn), Js.Nullable.t('a)) => unit) => unit,
    ~validate: 'a => bool,
    ~destroy: 'a => unit,
    unit
  ) =>
  t;

let acquire: t => Js.Promise.t(Belt.Result.t('a, exn));

let destroy: t => Js.Promise.t(unit);

let release: (t, 'a) => unit;

let numUsed: t => int;

let numFree: t => int;

let numPendingAcquires: t => int;

let numPendingCreates: t => int;
