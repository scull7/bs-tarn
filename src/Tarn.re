exception Acquire_failure(string);

module Options = {
  [@bs.deriving abstract]
  type t('a) = {
    create: ((Js.Nullable.t(exn), Js.Nullable.t('a)) => unit) => unit,
    validate: 'a => bool,
    destroy: 'a => unit,
    min: int,
    max: int,
    acquireTimeoutMillis: int,
    createTimeoutMillis: int,
    idleTimeoutMillis: int,
    reapIntervalMillis: int,
    createRetryIntervalMillis: int,
    propagateCreateError: bool,
  };

  let make = t;
};

type t;

[@bs.module "tarn"] [@bs.new] external make : Options.t('a) => t = "Pool";

let make =
    (
      ~min=?,
      ~max=?,
      ~acquireTimeoutMillis=?,
      ~createTimeoutMillis=?,
      ~idleTimeoutMillis=?,
      ~reapIntervalMillis=?,
      ~createRetryIntervalMillis=?,
      ~propagateCreateError=?,
      ~create,
      ~validate,
      ~destroy,
      _,
    ) =>
  Options.make(
    ~min=min |. Belt.Option.getWithDefault(2),
    ~max=max |. Belt.Option.getWithDefault(10),
    ~acquireTimeoutMillis=
      acquireTimeoutMillis |. Belt.Option.getWithDefault(3000),
    ~createTimeoutMillis=
      createTimeoutMillis |. Belt.Option.getWithDefault(3000),
    ~idleTimeoutMillis=idleTimeoutMillis |. Belt.Option.getWithDefault(3000),
    ~reapIntervalMillis=
      reapIntervalMillis |. Belt.Option.getWithDefault(1000),
    ~createRetryIntervalMillis=
      createRetryIntervalMillis |. Belt.Option.getWithDefault(200),
    ~propagateCreateError=
      propagateCreateError |. Belt.Option.getWithDefault(false),
    ~create,
    ~destroy,
    ~validate,
  )
  |. make;

[@bs.deriving abstract]
type resource('a) = {promise: Js.Promise.t('a)};

[@bs.send] external acquire : t => resource('a) = "";

[@bs.send] external destroy : t => Js.Promise.t(unit) = "";

[@bs.send] external release : (t, 'a) => unit = "";

[@bs.send] external numUsed : t => int = "";

/**
 * ## numFree
 * Returns the number of already initialized resources
 * which are free to be acquired.  Therefore, upon initialization of
 * the pool there will be zero free resources.
 */
[@bs.send]
external numFree : t => int = "";

[@bs.send] external numPendingAcquires : t => int = "";

[@bs.send] external numPendingCreates : t => int = "";

let acquire = t =>
  acquire(t)
  |> promiseGet
  |> Js.Promise.then_(resource =>
       resource |. Belt.Result.Ok |. Js.Promise.resolve
     )
  |> Js.Promise.catch(err =>
       err
       |. Js.String.make
       |. Acquire_failure
       |. Belt.Result.Error
       |. Js.Promise.resolve
     );
