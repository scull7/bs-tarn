open Ava;

module Resource = {
  type t = {
    name: string,
    active: bool,
  };

  let make = str => {name: str, active: true};

  let nameGet = ({name}) => name;

  let destroy = t => {...t, active: false};
};

let makePool = name =>
  Tarn.make(
    ~create=
      cb => cb(Js.Nullable.null, name |. Resource.make |. Js.Nullable.return),
    ~validate=_ => true,
    ~destroy=_ => (),
    (),
  );

let makeFailPool = _ =>
  Tarn.make(
    ~create=cb => cb(Js.Nullable.return(Not_found), Js.Nullable.null),
    ~validate=_ => true,
    ~destroy=_ => (),
    (),
  );

let pool = makePool("foo");

test("numUsed should return zero", t => {
  let count = pool |. Tarn.numUsed;
  Assert.true_(t, count == 0);
});

test("numFree should return zero", t => {
  let count = pool |. Tarn.numFree;
  Assert.true_(t, count == 0);
});

test("numPendingAcquires should return zero", t => {
  let count = pool |. Tarn.numPendingAcquires;
  Assert.true_(t, count == 0);
});

test("numPendingCreates should return zero", t => {
  let count = pool |. Tarn.numPendingCreates;
  Assert.true_(t, count == 0);
});

Async.test(ava, "acquire should return a resource", t =>
  pool
  |> Tarn.acquire
  |> Js.Promise.then_(resource => {
       resource
       |. Belt.Result.map(Resource.nameGet)
       |. Belt.Result.map(x => x == "foo")
       |. Belt.Result.map(x => x |> Assert.true_(t))
       |. Belt.Result.map(Tarn.release(pool))
       |. ignore;

       switch (resource) {
       | Belt.Result.Ok(_) => Test.finish(t)
       | Belt.Result.Error(exn) =>
         Test.fail(~message=Js.String.make(exn), t);
         Test.finish(t);
       };

       Js.Promise.resolve();
     })
  |> Js.Promise.catch(e => {
       Test.fail(~message=Js.String.make(e), t);
       Test.finish(t);
       Js.Promise.resolve();
     })
  |. ignore
);

Async.test(ava, "pool destroy should kill the pool", t =>
  makePool("destroy")
  |> Tarn.destroy
  |> Js.Promise.then_(_ => Test.pass(t) |> Js.Promise.resolve)
  |> Js.Promise.catch(e =>
       Test.fail(~message=Js.String.make(e), t) |> Js.Promise.resolve
     )
  |> Js.Promise.then_(_ => Test.finish(t) |> Js.Promise.resolve)
  |> ignore
);

Async.test(
  ava,
  "releasing an acquired resource",
  t => {
    let myPool = makePool("release");

    myPool
    |> Tarn.acquire
    |> Js.Promise.then_(result =>
         result
         |. Belt.Result.map(r => {
              Assert.true_(t, Tarn.numUsed(myPool) == 1) |. ignore;
              Tarn.release(myPool, r);
            })
         |. Belt.Result.map(_ => {
              Assert.true_(t, Tarn.numUsed(myPool) == 0) |. ignore;
              Assert.true_(t, Tarn.numFree(myPool) == 1) |. ignore;
            })
         |. ignore
         |. Js.Promise.resolve
       )
    |> Js.Promise.catch(e =>
         Test.fail(~message=Js.String.make(e), t) |> Js.Promise.resolve
       )
    |> Js.Promise.then_(_ => Test.finish(t) |> Js.Promise.resolve)
    |> Js.Promise.then_(_ => Tarn.destroy(myPool))
    |> ignore;
  },
);

Async.test(
  ava,
  "failure to acquire",
  t => {
    let failPool = makeFailPool("failure");

    failPool
    |> Tarn.acquire
    |> Js.Promise.then_(result => {
         switch (result) {
         | Belt.Result.Ok(s) => Test.fail(~message=Js.String.make(s), t)
         | Belt.Result.Error(_) => Test.pass(t)
         };
         Js.Promise.resolve();
       })
    |> Js.Promise.catch(_ => {
         Test.pass(t);
         Js.Promise.resolve();
       })
    |> Js.Promise.then_(_ => Test.finish(t) |> Js.Promise.resolve)
    |> Js.Promise.then_(_ => Tarn.destroy(failPool))
    |. ignore;
  },
);
