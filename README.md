[![pipeline status](https://gitlab.com/scull7/bs-tarn/badges/master/pipeline.svg)](https://gitlab.com/scull7/bs-tarn/commits/master)
[![coverage report](https://gitlab.com/scull7/bs-tarn/badges/master/coverage.svg)](https://scull7.gitlab.io/bs-tarn/)

# Build
```
npm run build
```

# Watch

```
npm run watch
```


# Editor
If you use `vscode`, Press `Windows + Shift + B` it will build automatically
